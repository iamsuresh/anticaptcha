# remember to install the library: pip install scraperapi-sdk
from scraper_api import ScraperAPIClient
client = ScraperAPIClient('55b7f5b32dea95250b27edce13221ced')
result = client.get(url = 'http://angel.co/login', render=True).text
print(result);
# Scrapy users can simply replace the urls in their start_urls and parse function
# Note for Scrapy, you should not use DOWNLOAD_DELAY and
# RANDOMIZE_DOWNLOAD_DELAY, these will lower your concurrency and are not
# needed with our API

# ...other scrapy setup code
start_urls =[client.scrapyGet(url = 'http://angel.co/login', render=True)]
def parse(self, response):

    result.find_element_by_id('user_email').send_keys(" ") # username

    result.find_element_by_id('user_password').send_keys(" ") #password

    result.find_element_by_name('commit').click() #submit